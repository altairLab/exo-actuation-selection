function draw_polygon_motorgear_tpe(values, axis_max_values)
    % Draw the polygon of the motorgear with the PE.
    
    % color of vertexes and edges
    edge_color = 'blue';
    % color of face
    face_color = 'blue';
    
    % input of exagon_vertex is the position of each vertex (the distance from the origin)
    vertexes = exagon_vertex(values ./ axis_max_values);

    % draw edges
    p = patch('Vertices', vertexes, 'Faces', 1:length(vertexes), ...
        'EdgeColor', edge_color, 'FaceColor', 'none');
    set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    
    % draw face
    p = patch('Vertices', vertexes, 'Faces', 1:length(vertexes), ...
        'EdgeColor', 'none', 'FaceColor', face_color);
    alpha(p, 0.1);
    set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

    % draw vertexes
    p = plot(vertexes(:,1), vertexes(:,2),...
        'o',...
        'MarkerSize', 10,...
        'MarkerEdgeColor', edge_color,...
        'MarkerFaceColor', edge_color, ...
        'DisplayName', 'motorgear reqs');
    
end