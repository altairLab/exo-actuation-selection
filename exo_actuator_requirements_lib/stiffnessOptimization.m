function [required_motor_torque_with_PE, PE_torque] = stiffnessOptimization(stiffness_profile_order_mode, required_motor_torque, task_data, ratio, mode) 
	% call the correct function
	switch stiffness_profile_order_mode
		case 1
			[required_motor_torque_with_PE, PE_torque] = stiffnessOptimization_poly1(required_motor_torque, task_data, ratio, mode);
		case 2
			[required_motor_torque_with_PE, PE_torque] = stiffnessOptimization_poly1_zero(required_motor_torque, task_data, ratio, mode);
		case 3
			[required_motor_torque_with_PE, PE_torque] = stiffnessOptimization_poly2_zero(required_motor_torque, task_data, ratio, mode);
		case 4    
			[required_motor_torque_with_PE, PE_torque] = stiffnessOptimization_poly4_zero(required_motor_torque, task_data, ratio, mode);
	end
end

