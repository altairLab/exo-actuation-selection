addpath('../../Data_IIT_lifting')
addpath('../../Data_IIT_walking\')

% Experiment Data load
matPathName = 'VUAms_lifting_no-exo.mat';
sbj_tags = {'subj7'; 'subj8'}; %subj6 == subj7
Ts = 0.02;

% torques is divided in the 2 hips and only 1/3 of the required torque is
% porvided by the exo -> 0.33/2
trial_cells = prepareTaskData( matPathName, sbj_tags, Ts, 0 , 0.33/2);

sbj_idx_list = [1 2];

trial1_table = table();
trial2_table = table();

for sbj_idx=sbj_idx_list
    trial1_table = [trial1_table; trial_cells{sbj_idx,1}; trial_cells{sbj_idx,2}; trial_cells{sbj_idx,3}];
    trial2_table = [trial2_table; trial_cells{sbj_idx,4}; trial_cells{sbj_idx,5}; trial_cells{sbj_idx,6}];
end

trial_all_table = [trial1_table; trial2_table];

t = [0:Ts:(size(trial_all_table,1)-1)*Ts]';
trial_all_table.time = t;
