motor.Name = 'EC60_24V';

motor.Kt = 0.0534; % Nm / A 
motor.Ra = 0.307; % ohm
J_gcm2 = 1210; % g cm2
motor.Jm = J_gcm2 / 1000 / 10000; %Kg m^2  
motor.Fv = 0.0; % Nm * s/rad
motor.Fc = 0.0; % Nm
motor.StallTorque = 4.18; %Nm
motor.StallCurrent = motor.StallTorque/motor.Kt; %A
motor.NominalTorque = 0.227; %Nm
motor.NominalCurrent = motor.NominalTorque/motor.Kt; %A
motor.NoLoadSpeed = 4250 * 2*pi / 60; % rad/s
motor.NoLoadCurrent = 0.419; % A
motor.MaxEfficiency = 0.85;
motor.MaxPower = 100; % W
motor.MaxVoltage = 24; % V

motor.R12 = 3.5; % �K/W
motor.R2A = 2.5; % �K/W
% motor.R2A = 4.34; % �K/W
motor.TimeConst1 = 40; % s
motor.TimeConst2 = 89; % s
% motor.TimeConst2 = 155; % s
motor.MaxWindingTemp = 125; % �C

motor.Weight = 0.47;	% Kg
motor.Radius = 0.068;	% m
motor.Height = 0.043;	% m
motor.Volume = pi * motor.Radius^2 * motor.Height;	% m^3