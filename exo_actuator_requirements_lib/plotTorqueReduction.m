% plot augmented task vs sugmented task with PE
f2 = figure(2);
hold on;

% plot the augmented task part
plot(pos_deg,augmented_task.motor_torque*transmission.ratio, ...
    'Color', [0 0.4 0]);

% plot the augmented task with PE part
plot(pos_deg,motor_requirements.motor_torque*transmission.ratio, ...
    'Color', 'blue', ...
    'DisplayName', 'motorgear torques');

% figure name
f_name = ['' motor.Name '_r' char(string(transmission.ratio)) '_torque_reduction_with_PA' char(string(stiffness_opt_mode))];

% labels and legend
lx = xlabel("$\theta [deg]$",'FontSize',12);
ly = ylabel("$[Nm]$",'FontSize',12);
leg = legend('$\tau_{a}$', '$\tau_{pe}$');
set(leg,'Interpreter','Latex');
set(lx,'Interpreter','Latex');
set(ly,'Interpreter','Latex');
leg.Location = 'northwest';
leg.FontSize = 14;
set(gca,'FontSize',14);
xlim_offset = (abs(max(pos_deg)) + abs(min(pos_deg)))*0.02; 
set(gca,'xlim',[min(pos_deg)-xlim_offset max(pos_deg)+xlim_offset]);

% save in .tiff and in .fig
print(f2,[stiffness_optimization_plot_dir f_name], '-dtiff', '-r0');
savefig(f2,[stiffness_optimization_plot_dir f_name]);
close(f2);

% create the next figure
colors = [[0 0 0];[1 0.8 0];[0 0 0.6];[0.6 0 0];[0 0.5 0.5]];
% colors = [[0 0 0];[0.2 0.2 0.2];[0.4 0.4 0.4];[0.8 0.8 0.8];[0 0 0]]; % grey scale

