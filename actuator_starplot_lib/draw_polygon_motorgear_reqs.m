function draw_polygon_motorgear_reqs(values, axis_max_values)
    % Draw the poligon of the task requirements considering the motor
    % inertia.
    
    % color of vertexes and edges
    edge_color = [0 0.4 0];

    % input of exagon_vertex is the position of each vertex (the distance from the origin)
    vertexes = exagon_vertex(values ./ axis_max_values);

    % draw edges
    p = patch('Vertices', vertexes, 'Faces', 1:length(vertexes), ...
        'EdgeColor', edge_color, 'FaceColor', 'none');
    set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

    % draw vertexes
    p = plot(vertexes(:,1), vertexes(:,2),...
        'o',...
        'MarkerSize', 10,...
        'MarkerEdgeColor', edge_color,...
        'MarkerFaceColor', edge_color, ...
        'DisplayName', 'task augmented reqs');

end
    