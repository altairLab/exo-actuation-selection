motor.Name = 'EC90_24V';

motor.Kt = 0.0705; % Nm / A 
motor.Ra = 0.343; % ohm
J_gcm2 = 3060; % g cm2
motor.Jm = J_gcm2 / 1000 / 10000; %Kg m^2  
motor.Fv = 0.0; % Nm * s/rad
motor.Fc = 0.0; % Nm
motor.StallTorque = 4.940; %Nm
motor.StallCurrent = motor.StallTorque/motor.Kt; %A
motor.NominalTorque = 0.444; %Nm
motor.NominalCurrent = motor.NominalTorque/motor.Kt; %A
motor.NoLoadSpeed = 3190 * 2*pi / 60;  % rad/s
motor.NoLoadCurrent = 0.544; % A
motor.MaxEfficiency = 0.84;
motor.MaxPower = 90; % W
motor.MaxVoltage = 24; % V

motor.R12 = 2.6; % �K/W
motor.R2A = 1.91; % �K/W
motor.TimeConst1 = 46; % s
motor.TimeConst2 = 283; % s
motor.MaxWindingTemp = 125; % �C

motor.Weight = 0.6;	% Kg
motor.Radius = 0.09;	% m
motor.Height = 0.027;	% m
motor.Volume = pi * motor.Radius^2 * motor.Height;	% m^3