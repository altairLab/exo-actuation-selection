% create the experiments that will be saved in the .mat file
experiments = [];
experiment_index = 1;

% save all the directories in the experiments
experiments.csv_dir = csv_dir;
experiments.csv_name = csv_name;
experiments.simulation_output_dir = simulation_output_dir;
experiments.simulation_results_filename = simulation_results_filename;
experiments.stiffness_optimization_plot_dir = stiffness_optimization_plot_dir;
experiments.starplot_dir = starplot_dir;
experiments.starplot_min_dir = starplot_miniature_dir;

% add motors and transmissions to experiments
experiments.motors = motors;
experiments.transmissions = transmissions;

% add stiffness opt modes and mode names to experiments
experiments.stiffness_opt_mode_names = stiffness_opt_mode_names;
experiments.stiffness_opt_modes = stiffness_opt_modes;

% add stiffness order profile
experiments.stiffness_profile_order_mode = stiffness_profile_order_mode;
experiments.stiffness_profile_order_names = stiffness_profile_order_names;
        
for m_ = motors
    for t_ = transmissions
        fprintf('%s',['motor: ' m_ ' - transmission: ' t_])
        fprintf('\n')
        
        close all;
        
        % load motor from the correct file in 'configurations/'
        run(m_);
        % load transmission from the correct file in 'configurations/'
        run(t_);
        
        % add motor and transmission to the current experiment
        experiments(experiment_index).motor = motor;
        experiments(experiment_index).transmission = transmission;
        
        % Motor-Gear limits t output shaft by datasheet
        motorgear.max_speed = min([transmission.MaxSpeed motor.NoLoadSpeed]) / ...
            transmission.ratio;
        motorgear.avg_speed = transmission.AvgSpeed / transmission.ratio;
        motorgear.stall_torque = min([ ...
            (motor.StallTorque * transmission.ratio), ...
            transmission.MaxTorque]);
        motorgear.nominal_torque = motor.NominalTorque * transmission.ratio;
        motorgear.avg_torque = transmission.AvgTorque;
        motorgear.heating = motor.MaxWindingTemp;
        motorgear.max_voltage = motor.MaxVoltage;
		motorgear.total_inertia = (motor.Jm + transmission.J) * transmission.ratio;
		motorgear.total_weight = motor.Weight + transmission.Weight;
		motorgear.total_volume = motor.Volume + transmission.Volume;
        % motorgear.max_power = motorgear.stall_torque * motorgear.max_speed;
        % motorgear.dissipation = motor.NominalCurrent^2 * motor.Ra;
        
        % add motorgear to the current experiment
        experiments(experiment_index).motorgear = motorgear;
        
        
        % take task constraints
        task.max_speed = max(abs(task.task_data.velocity));
        task.avg_speed = mean(abs(task.task_data.velocity));
        task.max_torque = max(abs(task.task_data.torque));
        task.avg_torque = mean(abs(task.task_data.torque));
        %task.requirements_max_power = max((task.requirements_power));
        
        % add task to the current experiment
        experiments(experiment_index).task = task;
        
        
        % Compute motor gear requirements to support the augmented task (considering motor and gear efficiency)
        augmented_task.motor_torque = computeAugmentedMotorTorque(motor, transmission, task.task_data);
        augmented_task.motor_current = augmented_task.motor_torque / motor.Kt;
        augmented_task.voltage = motor.Ra * augmented_task.motor_current + ...
            motor.Kt * task.task_data.velocity * transmission.ratio;
        augmented_task.max_speed = task.max_speed;
        augmented_task.avg_speed = task.avg_speed;
        augmented_task.max_torque = max(abs(augmented_task.motor_torque * transmission.ratio));
        augmented_task.avg_torque = mean(abs(augmented_task.motor_torque * transmission.ratio));
        augmented_task.max_mech_power = max((augmented_task.motor_torque * transmission.ratio .* task.task_data.velocity));
        augmented_task.max_voltage = max(abs(augmented_task.voltage));
        augmented_task.max_electric_power = max((augmented_task.motor_current .* augmented_task.voltage));
        
        % motorgear requirements (relaxed by PE) - Structs preallocation
        motorgear_requirements.max_speed    = 0;
        motorgear_requirements.avg_speed    = 0;
        motorgear_requirements.max_torque   = 0;
        motorgear_requirements.avg_torque   = 0;
        motorgear_requirements.max_power    = 0;
        motorgear_requirements.dissipation  = 0;
        motorgear_requirements.max_voltage  = 0;
        motorgear_requirements.max_heating  = 0;
        motorgear_requirements.max_mechanical_power  = 0;
        motorgear_requirements.max_electric_power  = 0;
        motorgear_requirements_datas = motorgear_requirements;
        
        % Motorgear Thermal Model
        global R12 R2A tau1 tau2 Ra
        R12 = motor.R12; % �K/W
        R2A = motor.R2A; % �K/W
        tau1 = motor.TimeConst1; % s
        tau2 = motor.TimeConst2; % s
        Ra = motor.Ra;
        
        % Thermal SIMULATION whithout PE
        clear x
        clear power_in
        power_in.time = [0];
        power_in.signals.values = [];
        power_in.signals.values = 1;

        % task concatenation over simulation time
        task_number_values = length(task.task_data.time);
        repeat = round(evaluation_period/task_periodicity);
        period_length = floor(task_periodicity/task.ts);
        for i=1:repeat
            % create the new period values (with leading zeros when the
            % task is finished)
            new_period_values = zeros(period_length,1);
            new_period_values(1:task_number_values,1) = motor.Ra*augmented_task.motor_current.^2;
            
            % assign the new period to power in
            power_in.signals.values = [power_in.signals.values; new_period_values];
            
            % update the time
            power_in.time = [power_in.time; power_in.time(end) + (task.ts:period_length)'];
        end
        
        [t, x, outs] = sim([actuator_requirements_lib_dir '/thermal.mdl'], evaluation_period);
        
        % save results of the simulation without PE
        winding_temp_motorgear = x(:,1) + 25; % 25 deg is environment temperature
        augmented_task.max_heating = max(winding_temp_motorgear(1:floor(evaluation_period/simulation_step*0.8),1)); % we take the max discarding the last 20% of data (some spikes occours)
        
        % add augmented task to the current experiment
        experiments(experiment_index).augmented_task = augmented_task;
        % add winding temp motorgear to the current experiment
        experiments(experiment_index).winding_temp_motorgear = winding_temp_motorgear;
        
       
        for stiffness_opt_index = stiffness_opt_modes'
            
%             fprintf('%s',[stiffness_opt_mode_names(stiffness_opt_index) ' ' ])
%             fprintf('\n')
            
            % add stiffness opt mode to the current experiment and stiffness
            experiments(experiment_index).with_PE(stiffness_opt_index).stiffness_opt_mode = stiffness_opt_modes(stiffness_opt_index);
                        
            % STIFFNESS OPTIMIZATIONS
            [motor_requirements.motor_torque, PE_torque] = ...
				stiffnessOptimization(stiffness_profile_order_mode, ...
				augmented_task.motor_torque, task.task_data, ...
				transmission.ratio, stiffness_opt_modes(stiffness_opt_index));

            % add PE torque to the current experiment and stiffness
            experiments(experiment_index).with_PE(stiffness_opt_index).PE_torque = PE_torque;
            
            
            motor_requirements.max_torque = max(abs(motor_requirements.motor_torque));
            motor_requirements.motor_current = motor_requirements.motor_torque / motor.Kt;
            motor_requirements.motor_max_current = max(abs(motor_requirements.motor_current));
            motor_requirements.motor_mechanical_power = motor_requirements.motor_torque .* (transmission.ratio * task.task_data.velocity) ;
            motor_requirements.motor_electric_power = motor_requirements.motor_mechanical_power + motor.Ra * motor_requirements.motor_current.^2;
            motor_requirements.max_electric_power = max(motor_requirements.motor_electric_power);
            motor_requirements.mean_electric_power = mean(motor_requirements.motor_electric_power);    
            
            % add augmented task with PE to the current experiment and stiffness
            experiments(experiment_index).with_PE(stiffness_opt_index).motor_requirements = motor_requirements;

            % Motorgear requirements (considering the effort of parallel elastic element)
            motorgear_requirements.max_speed = task.max_speed;
            motorgear_requirements.avg_speed = task.avg_speed;
            motorgear_requirements.max_torque = motor_requirements.max_torque * ...
                transmission.ratio;
            motorgear_requirements.avg_torque = mean(abs(motor_requirements.motor_torque * ...
                transmission.ratio));
            motorgear_requirements.dissipation = mean(motor_requirements.motor_current.^2) * ...
                motor.Ra;
            motorgear_requirements_voltage = motor.Ra / motor.Kt * ...
                motor_requirements.motor_torque + ...
                motor.Kt * task.task_data.velocity * transmission.ratio;
            motorgear_requirements.max_voltage = max(abs(motorgear_requirements_voltage));
            motorgear_requirements.max_mechanical_power = max(motor_requirements.motor_mechanical_power);
            motorgear_requirements.max_electric_power = max(motor_requirements.motor_electric_power);            
            
            
            % Thermal SIMULATION with PE
            clear x
            clear power_in
            power_in.time = [0];
            power_in.signals.values = [];
            power_in.signals.values = 1;
            
            % task concatenation over simulation time
            task_number_values = length(task.task_data.time);
            repeat = round(evaluation_period/task_periodicity);
            period_length = floor(task_periodicity/task.ts);
            for i=1:repeat
                % create the new period values (with leading zeros when the
                % task is finished)
                new_period_values = zeros(period_length,1);
                new_period_values(1:task_number_values,1) = motor.Ra*motor_requirements.motor_current.^2;

                % assign the new period to power in
                power_in.signals.values = [power_in.signals.values; new_period_values];

                % update the time
                power_in.time = [power_in.time; power_in.time(end) + (task.ts:period_length)'];
            end
            
            [t, x, outs] = sim([actuator_requirements_lib_dir '/thermal.mdl'], evaluation_period);
            
            % save results of the current simulation with PE
            winding_temp_motorgear_with_PE = x(:,1);
            motorgear_requirements.max_heating = max(winding_temp_motorgear_with_PE(1:floor(evaluation_period/simulation_step*0.8),1));
            motorgear_requirements_datas(stiffness_opt_modes(stiffness_opt_index)) = motorgear_requirements;
            
            % add motor requirements to the current experiment and stiffness
            experiments(experiment_index).with_PE(stiffness_opt_index).motorgear_requirements = motorgear_requirements;
            % add winding temp motorgear to the current experiment
            experiments(experiment_index).with_PE(stiffness_opt_index).winding_temp_motorgear_with_PE = winding_temp_motorgear_with_PE;
            % add motorgear requirements data to the current experiment
            experiments(experiment_index).with_PE(stiffness_opt_index).motorgear_requirements_datas = motorgear_requirements_datas;
            
        end
        
        experiment_index = experiment_index + 1;
    end
end

% save the workspace in the simulation results file
save([simulation_output_dir simulation_results_filename],'experiments');
clearvars -except simulation_output_dir simulation_results_filename
