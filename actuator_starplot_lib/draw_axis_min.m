function draw_axis_min()

    % input of exagon_vertex is the length of the axis
    polygon_vertexes = exagon_vertex(2.0);

    % loop for each vertex
    for i=1:length(polygon_vertexes)

        % dashed lines black for the axis
        p = plot( [0 polygon_vertexes(i,1)], [0 polygon_vertexes(i,2)], '--k' );
        set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

        % axis ends with a black circle
        p = plot( polygon_vertexes(i,1), polygon_vertexes(i,2), ...
            'ok', ...
            'MarkerEdgeColor', 'black', ...
            'MarkerFaceColor', 'black');
        set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end

end