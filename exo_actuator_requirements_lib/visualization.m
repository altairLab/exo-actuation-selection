function visualization(simulation_output_dir,simulation_results_filename) 
% load the saved workspace from the simulation results file
load([simulation_output_dir simulation_results_filename]);

% load all the directories
csv_dir = experiments.csv_dir;
csv_name = experiments.csv_name;
simulation_output_dir = experiments.simulation_output_dir;
simulation_results_filename = experiments.simulation_results_filename;
stiffness_optimization_plot_dir = experiments.stiffness_optimization_plot_dir;
starplot_dir = experiments.starplot_dir;
starplot_min_dir = experiments.starplot_min_dir;

% load motors and transmissions
motors = experiments.motors;
transmissions = experiments.transmissions;

% load stiffness opt modes and mode names
stiffness_opt_mode_names = experiments.stiffness_opt_mode_names;
stiffness_opt_modes = experiments.stiffness_opt_modes;

% load stiffness order profile
stiffness_profile_order_mode = experiments.stiffness_profile_order_mode;
stiffness_profile_order_names = experiments.stiffness_profile_order_names;

% loop for the simulations
for experiment_index = 1:length(experiments)

    close all;

    % add motor and transmission to the current experiment
    motor = experiments(experiment_index).motor;
    transmission = experiments(experiment_index).transmission;
    motorgear = experiments(experiment_index).motorgear;
    task = experiments(experiment_index).task;
    augmented_task = experiments(experiment_index).augmented_task;
    winding_temp_motorgear = experiments(experiment_index).winding_temp_motorgear;
    pos_deg = rad2deg(task.task_data.position);
    
    % PLOT: task vs augmented task torque
    plotTaskVsAugmentedTaskTorque
    
    % PLOT: stiffness optimizations on the augmented task
    f55 = figure(1); % stifness profile figure 
    plotStiffnessProfileOnAugmentedTask
    %colors = [[0 0 0];[1 0.8 0];[0 0 0.6];[0.6 0 0];[0 0.5 0.5]];
    colors = [[0 0 0];[0.2 0.2 0.2];[0.4 0.4 0.4];[0.8 0.8 0.8];[0 0 0]]; % grey scale

        
    
    % loop for the visualization with PE
    for stiffness_index = 1:length(experiments(experiment_index).with_PE)

        stiffness_opt_mode = experiments(experiment_index).with_PE(stiffness_index).stiffness_opt_mode;
        motor_requirements = experiments(experiment_index).with_PE(stiffness_index).motor_requirements;
        motorgear_requirements = experiments(experiment_index).with_PE(stiffness_index).motorgear_requirements;
        winding_temp_motorgear_with_PE = experiments(experiment_index).with_PE(stiffness_index).winding_temp_motorgear_with_PE;
        motorgear_requirements_datas = experiments(experiment_index).with_PE(stiffness_index).motorgear_requirements_datas;
        PE_torque = experiments(experiment_index).with_PE(stiffness_index).PE_torque;

        % PLOT: augmented task vs augmented task with PE
        %plotTorqueReduction
        
        % PLOT: stiffness optimizations on the augmented task
        figure(f55); hold on;
        plot(pos_deg,PE_torque, ...
            'LineWidth', 3, ...
            'Color', colors(stiffness_opt_mode, :), ...
            'DisplayName', stiffness_opt_mode_names(stiffness_opt_mode));


        % PLOT: Small Motor STARPLOT
        motor_starplot_min
        close(f10);
        
        % PLOT: Motor STARPLOT
        motor_starplot
        close(f100);
    end
    
   
    figure(f55);
    f_name = ['' motor.Name '_r' char(string(transmission.ratio)) '_stifness_profile'];
    print(f55,[stiffness_optimization_plot_dir f_name], '-dtiff', '-r0');
    savefig(f55,[stiffness_optimization_plot_dir f_name]);
    close(f55);

end