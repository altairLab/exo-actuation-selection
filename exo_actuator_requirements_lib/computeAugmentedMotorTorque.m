function tau_m = computeAugmentedMotorTorque(motor, transmission, task_data)

theta = task_data.position * transmission.ratio;
dtheta = task_data.velocity * transmission.ratio;
ddtheta = task_data.acceleration * transmission.ratio;

efficiency = (transmission.ratio*transmission.efficiency*motor.MaxEfficiency);

tau_m = task_data.torque + ...
        motor.Jm * ddtheta + ...
        motor.Fv * dtheta + ...
        motor.Fc * sign(dtheta) + ...
        transmission.J * ddtheta + ...
        transmission.Fv * task_data.velocity + ...
        transmission.Fc * sign(task_data.velocity);
    
 tau_m = tau_m / efficiency; 
