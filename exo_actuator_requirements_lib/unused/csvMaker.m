% load data with taskDataLoad
taskDataLoad

% create the csv directory if doesn't exist
csv_dir = '../../csv_data/';
if(~exist(csv_dir, 'dir'))
   mkdir(csv_dir)
end

% insert data in a matrix
matrix4csv(:,1) = trial_all_table.time;
matrix4csv(:,2) = trial_all_table.torque;
matrix4csv(:,3) = trial_all_table.position;
matrix4csv(:,4) = trial_all_table.velocity;
matrix4csv(:,5) = trial_all_table.acceleration;

% convert the matrix in table
table4csv = array2table(matrix4csv,'VariableNames',{'time','torque','position','velocity','acceleration'});

% make the csv filename 
csv_name = [csv_dir 'data1_ts=' num2str(Ts) '.csv']

% save the table to the csv file
writetable(table4csv,csv_name);




% %% Alternative
% % make the csv filename 
% csv_name = [csv_dir 'data1_ts=' num2str(Ts) '.csv']
% 
% % save the table to the csv file
% writetable(trial_all_table,csv_name);


