function p = quadrangle_vertex(points)

if length(points) == 1
    points = [1.0 1.0 1.0 1.0] * points;
end
    
if length(points) >= 1
    p(1,1) = 0.0;
    p(1,2) = points(1);
end
if length(points) >= 2
    p(2,1) = -points(2);
    p(2,2) = 0.0;
end
if length(points) >= 3
    p(3,1) = 0.0;
    p(3,2) = -points(3);
end
if length(points) >= 4
    p(4,1) = points(4);
    p(4,2) = 0.0;
end
   
end