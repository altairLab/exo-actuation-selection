motor.Name = 'EC45_24V';

motor.Kt = 0.0369; % Nm / A 
motor.Ra = 0.608; % ohm
J_gcm2 = 181; % g cm2
motor.Jm = J_gcm2 / 1000 / 10000; %Kg m^2  
motor.Fv = 0.0; % Nm * s/rad
motor.Fc = 0.0; % Nm
motor.StallTorque = 1.460; %Nm
motor.StallCurrent = motor.StallTorque/motor.Kt; %A
motor.NominalTorque = 0.128; %Nm
motor.NominalCurrent = motor.NominalTorque/motor.Kt; %A
motor.NoLoadSpeed = 6110 * 2*pi / 60;  % rad/s
motor.NoLoadCurrent = 0.234; % A
motor.MaxEfficiency = 0.85;
motor.MaxPower = 70; % W
motor.MaxVoltage = 24; % V

motor.R12 = 4.1; % �K/W
motor.R2A = 3.56; % �K/W
motor.TimeConst1 = 29.6; % s
motor.TimeConst2 = 178; % s
motor.MaxWindingTemp = 125; % �C

motor.Weight = 0.141;	% Kg
motor.Radius = 0.043;	% m
motor.Height = 0.027;	% m
motor.Volume = pi * motor.Radius^2 * motor.Height;	% m^3