transmission.ratio = 100;

transmission.efficiency = 0.70;
transmission.Fv = 0*2/transmission.ratio; % Nm * s/rad
transmission.Fc = 0*2/transmission.ratio; % Nm
transmission.J = 0.09*1e-4; %Kg m^2
transmission.AvgSpeed = 3500 * 2*pi / 60;
transmission.MaxSpeed = 6500 * 2*pi / 60; % Maximum input speed
% transmission.MaxTorque = 95; % Momentary peak torque% 57; % 34 % Nm
transmission.MaxTorque = 57; % Repeated peak torque % 34 % Nm
transmission.AvgTorque = 34; % Avarage torque % Nm

transmission.Weight = 0.52;	% Kg
transmission.Radius = 0.0095;	% m
transmission.Height = 0.09;	% m
transmission.Volume = pi * transmission.Radius^2 * ...
	transmission.Height;	% m^3