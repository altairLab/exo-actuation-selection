function [required_motor_torque_with_PE, PE_torque] = stiffnessOptimization_poly1(required_motor_torque, task, ratio, mode) 

pos = task.position;
vel = task.velocity;

switch mode
    case 1
        [required_motor_torque_with_PE, PE_torque] = stiffnessOptimization_max_residue_torque(required_motor_torque, pos, ratio);
    case 2
        [required_motor_torque_with_PE, PE_torque] = stiffnessOptimization_mean_sqr_current(required_motor_torque, pos, ratio);
    case 3
        [required_motor_torque_with_PE, PE_torque] = stiffnessOptimization_max_mechanical_power(required_motor_torque, pos, vel, ratio);
    case 4    
        [required_motor_torque_with_PE, PE_torque] = stiffnessOptimization_mean_mechanical_power(required_motor_torque, pos, vel, ratio);
    case 5
        [required_motor_torque_with_PE, PE_torque] = stiffnessOptimizationNone(required_motor_torque, pos, ratio);
end






function [required_motor_torque_with_PE, PE_torque] = stiffnessOptimization_mean_sqr_current(required_motor_torque, pos, ratio)

y = required_motor_torque * ratio; 
phi = [ones(length(pos),1) pos];
[lambda] = LSIdentification(phi,y, 3, 1);

PE_torque = (lambda(1) + pos*lambda(2));
required_motor_torque_with_PE = required_motor_torque - (PE_torque / ratio);


function [required_motor_torque_with_PE, PE_torque] = stiffnessOptimization_max_residue_torque(required_motor_torque, pos, ratio)

phi = [ones(length(pos),1) pos];
y = required_motor_torque * ratio; 
cvx_begin
   variable lambda(2);
   minimize( norm(phi*lambda-y,Inf) );
cvx_end
PE_torque = (lambda(1) + pos*lambda(2));
required_motor_torque_with_PE = required_motor_torque - (PE_torque / ratio);


function [required_motor_torque_with_PE, PE_torque] = stiffnessOptimization_max_mechanical_power(required_motor_torque, pos, vel, ratio)

phi = [ones(length(pos),1) pos];
y = required_motor_torque * ratio; 
cvx_begin
   variable lambda(2);
   minimize( norm((phi*lambda-y).*vel,Inf) );
cvx_end
PE_torque = (lambda(1) + pos*lambda(2));
required_motor_torque_with_PE = required_motor_torque - (PE_torque / ratio);


function [required_motor_torque_with_PE, PE_torque] = stiffnessOptimization_mean_mechanical_power(required_motor_torque, pos, vel, ratio)

phi = [ones(length(pos),1) pos];
y = required_motor_torque * ratio; 
cvx_begin
   variable lambda(2);
   minimize( norm((phi*lambda-y).*vel,1) );
cvx_end
PE_torque = (lambda(1) + pos*lambda(2));
required_motor_torque_with_PE = required_motor_torque - (PE_torque / ratio);


function [required_motor_torque_with_PE, PE_torque] = stiffnessOptimizationNone(required_motor_torque, pos, ratio)

PE_torque = zeros(length(pos),1);
required_motor_torque_with_PE = required_motor_torque;
