function [ trial_cells ] = prepareTaskData( matPathName, sbj_tags, Ts, plot_on, torque_ratio )
%LOAD_VU_DATASET Summary of this function goes here
%   Detailed explanation goes here
repetitions_per_sbj = 6;
s = load(matPathName);
trial_cells = cell(length(sbj_tags),repetitions_per_sbj);

for tag_idx=1:length(sbj_tags)
    data_raw = s.(sbj_tags{tag_idx});

    for i=1:repetitions_per_sbj
        trial = data_raw(i);
        torque = trial.peMl5s1(3:end-2,2) * torque_ratio;
        pos_raw = trial.flexion_angle*pi/180;
        pos = pos_raw(3:end-2);
        vel_raw = (pos_raw(3:end)-pos_raw(1:end-2))/(2*Ts);
        vel = vel_raw(2:end-1);
        acc = (vel_raw(3:end)-vel_raw(1:end-2))/(2*Ts);
        time = [0:Ts:(length(pos)-1)*Ts]';
        trial_table = table(time,torque,pos,vel,acc,'VariableNames',{'time','torque','position','velocity','acceleration'});
        trial_cells{tag_idx,i} = trial_table;
        if plot_on
            figure
            hold on
            grid on
            plot(time,torque)
            plot(time,pos)
            plot(time,vel)
        end
    end
end
