function task = csvLoader(configuration_filename)
    % csvLoader: read a csv file and return the task data.
    
    % retrieve csv_dir and csv_name
    run(configuration_filename);
    
    % exit when there is a correct filename
    while(true)
        % make the filename
        filename = [csv_dir csv_name];
        
        % take the value of ts if the filename contains "*_ts='value'*
        C = regexp(csv_name,'(?<=_ts=)\d+\.?\d*','match');

        % check if there is ts and then save it, otherwise throw an error
        if(~isempty(C))
            task.ts = str2num(cell2mat(C(1,1)));
            break;
        else
            warning(['The "' filename '" filename is not in the' ...
                ' correct form: ''*_ts=value*'' (value in seconds).' ...
                sprintf('\nType a correct file and don''t forget to ') ...
                'change it in configuration.m ' ...
                'for future runs. (press CTRL-C to exit)']);
            csv_name = input('Please type a correct file, or press CTRL-C to exit: ','s');
        end
    end
    
    % read the table from the csv
    task.task_data = readtable(filename);
    
    
end






% %% TBD alternative: save in a vector all the files with respective Ts? or give the exact name of the .csv file?
% csv_dir = '../../csv_data/';
% filenames = dir([csv_dir '*.csv']);
% 
% count = 0;
% 
% for i = 1:length(filenames)
%     if(filenames(i).isdir == 0)
%         csv_name = [csv_dir filenames(i).name];
%         table = readtable(csv_name);
%         
%         % take the value of ts if the filename is "*_ts='value'*
%         C = regexp(csv_name,'(?<=_ts=)\d+\.?\d*','match');
%         
%         % if ts exists save it, otherwise throw an error
%         if(~isempty(C))
%             ts = str2num(cell2mat(C(1,1)));
%             count = count + 1;
%         else
%             error(['The "' csv_name '" filename is not in the correct ' ...
%                 'form: ''*_ts=value*''.' ...
%                 sprintf('\nPlease select a correct file.')]);
%         end
%     end
% end
% 
% experiment.trials = table;
% experiment.ts = ts;