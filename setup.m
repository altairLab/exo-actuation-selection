% input and libraries path 
simulation_input_dir = 'input/'; % path for actuator data, task data, and configurations 
actuator_starplot_lib_dir = 'actuator_starplot_lib/'; % library for plotting starplot
actuator_requirements_lib_dir = 'exo_actuator_requirements_lib/'; % library for plotting starplot
cvx_lib_dir = 'cvx_lib/'; % library spring optimization

% add necessary paths
addpath(simulation_input_dir);
addpath(actuator_starplot_lib_dir); 
addpath(actuator_requirements_lib_dir); 
% addpath('cvx_lib/'); % (not necessary if cvx is installed in MATLAB)
cvx_quiet true % remove console output of cvx

% load configuration
run(configuration_filename)

% make directories if don't exist
if(~exist(simulation_output_dir, 'dir')), mkdir(simulation_output_dir), end
if(~exist(stiffness_optimization_plot_dir, 'dir')), mkdir(stiffness_optimization_plot_dir), end
if(~exist(starplot_dir, 'dir')), mkdir(starplot_dir), end
if(~exist(starplot_miniature_dir, 'dir')), mkdir(starplot_miniature_dir), end


% load task data
task = csvLoader(configuration_filename);
