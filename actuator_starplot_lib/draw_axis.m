function draw_axis(axis_max_values, axis_labels)

    axis_max_values = axis_max_values .* 2;
    axis_labels(:,2) = string(axis_max_values)';

    % input of exagon_vertex is the length of the axis
    polygon_vertexes = exagon_vertex(2.0);

    % loop for each vertex
    for i=1:length(polygon_vertexes)

        % dashed lines black for the axis
        p = plot( [0 polygon_vertexes(i,1)], [0 polygon_vertexes(i,2)], '--k' );
        set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

        % axis ends with a black circle
        p = plot( polygon_vertexes(i,1), polygon_vertexes(i,2), ...
            'ok', ...
            'MarkerEdgeColor', 'black', ...
            'MarkerFaceColor', 'black');
        set(get(get(p,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    end

    font_size = 20;
    label_offset = 0.05;

    % draw all the labels
    text( polygon_vertexes(1,1) + label_offset, polygon_vertexes(1,2), axis_labels(1, :),...
            'HorizontalAlignment', 'left', 'FontSize', font_size, 'Interpreter','Latex');
    text( polygon_vertexes(2,1) - label_offset, polygon_vertexes(2,2), axis_labels(2, :),...
            'HorizontalAlignment', 'right', 'FontSize', font_size, 'Interpreter','Latex');
    text( polygon_vertexes(3,1) - label_offset, polygon_vertexes(3,2), axis_labels(3,:),...
            'HorizontalAlignment', 'right', 'FontSize', font_size, 'Interpreter','Latex');
    text( polygon_vertexes(4,1) + label_offset, polygon_vertexes(4,2), axis_labels(4,:),...
            'HorizontalAlignment', 'left', 'FontSize', font_size, 'Interpreter','Latex');
    text( polygon_vertexes(5,1) + label_offset, polygon_vertexes(5,2), axis_labels(5,:),...
            'HorizontalAlignment', 'left', 'FontSize', font_size, 'Interpreter','Latex');
    text( polygon_vertexes(6,1) + label_offset, polygon_vertexes(6,2), axis_labels(6,:),...
            'HorizontalAlignment', 'left', 'FontSize', font_size, 'Interpreter','Latex');

end
