% plot task vs augmented task
f1 = figure(1);
width = 570*1.5;
height = 413;
set(gcf,'position',[200,200,width,height])
hold off;

plot(pos_deg,task.task_data.torque, ...
    'Color', [0 0.8 0]);
hold on;
plot(pos_deg,augmented_task.motor_torque*transmission.ratio, ...
    'Color', [0 0.4 0]);
leg = legend('$\tau$');
lx = xlabel("$\theta [deg]$",'FontSize',12);
ly = ylabel("$[Nm]$",'FontSize',12);

set(leg,'Interpreter','Latex');
set(lx,'Interpreter','Latex');
set(ly,'Interpreter','Latex');
leg.Location = 'southeast';
leg.FontSize = 14;
xlim_offset = (abs(max(pos_deg)) + abs(min(pos_deg)))*0.02; 
set(gca,'xlim',[min(pos_deg)-xlim_offset max(pos_deg)+xlim_offset]);

set(gca,'FontSize',14);
f_name = ['' motor.Name '_r' char(string(transmission.ratio)) '_task_vs_augmented_task'];
print(f1,[stiffness_optimization_plot_dir  f_name], '-dtiff', '-r0');
savefig(f1,[stiffness_optimization_plot_dir  f_name]);