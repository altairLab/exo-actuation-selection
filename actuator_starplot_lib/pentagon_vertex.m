function p = pentagon_vertex(points)

if length(points) == 1
    points = [1.0 1.0 1.0 1.0 1.0] * points;
end
    
if length(points) >= 1
    p(1,1) = 0.0;
    p(1,2) = points(1);
end
if length(points) >= 2
    p(2,1) = cos(2.8274)*points(2);
    p(2,2) = sin(2.8274)*points(2);
end
if length(points) >= 3
    p(3,1) = cos(-2.1991)*points(3);
    p(3,2) = sin(-2.1991)*points(3);
end
if length(points) >= 4
    p(4,1) = cos(-0.9425)*points(4);
    p(4,2) = sin(-0.9425)*points(4);
end
if length(points) >= 5
    p(5,1) = cos(0.3142)*points(5);
    p(5,2) = sin(0.3142)*points(5);
end
   
end