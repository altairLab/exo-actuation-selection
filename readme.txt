****************
* INTRODUCTION *
****************

When selecting actuators for assistive exoskeletons it is generally desirable to avoid oversizing actuators in order to obtain more lightweight and transparent systems. 
In many cases, the torque and power requirements can be relaxed by exploiting the contribution of an elastic element acting in mechanical parallel. 
This software package supports the methodology proposed in [1] to match actuator capabilities to task requirements. 
Such methodology is based on a graphical tool showing how different design choices affect the actuator as a whole. 
In particular this matlab package provides a graphical evaluation of different actuator choices resulting from the combination of different motors, 
reduction gears, and parallel stiffness profiles.



****************
* INSTALL      *
****************

Run 'install.m' script to install cvx library properly.



****************
* RUN          *
****************

In 'configurations/configuration.m' one can set

- the considered actuators and transmissions data retrieved from datasheets, se for example EC45_24V_DataSheet.m and SHD_20_50_2SH_DataSheet

- stiffness optimization criteria

- output directories for star plots (in .tiff and .fig format) and for recording simulation results (in .mat). Change the latter when you want to run multiple simulations whitout overviriting previous simulation results

- the location of data recorded for the task prepared in a .csv file with the following format: time[s], position[rad], velocity[rad/s], acceleration[rad/s^2]) 

- the repetition freqency of the task. If the recorded task lasts XX seconds and should be repeated maximum 45 times in 1 hour set:

evaluation_period = 60*60; % 1 hour
task_periodicity = max(evaluation_period/45, XX);

 
Then, run 'main.m'.

To generate and visualize plot of a recorded simulation without running it again launch: 
visualize(simulation_results_filename) 
