function p = exagon_vertex(points)

if length(points) == 1
    points = [1.0 1.0 1.0 1.0 1.0 1.0] * points;
end
    
v = deg2rad(30);

if length(points) >= 1
    p(1,1) = 0.0;
    p(1,2) = points(1);
end
if length(points) >= 2
    p(2,1) = -cos(v)*points(2);
    p(2,2) = sin(v)*points(2);
end
if length(points) >= 3
    p(3,1) = -cos(v)*points(3);
    p(3,2) = -sin(v)*points(3);
end
if length(points) >= 4
    p(4,1) = 0.0;
    p(4,2) = -points(4);
end
if length(points) >= 5
    p(5,1) = cos(v)*points(5);
    p(5,2) = -sin(v)*points(5);
end
if length(points) >= 6
    p(6,1) = cos(v)*points(6);
    p(6,2) = sin(v)*points(6);
end
   
end