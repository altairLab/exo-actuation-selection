clear all

% choose the configuration file
configuration_filename = 'configuration_lowerlimb_iit.m';

% initial setup
setup

% simulation
simulation

% visualization
visualization(simulation_output_dir, simulation_results_filename) 