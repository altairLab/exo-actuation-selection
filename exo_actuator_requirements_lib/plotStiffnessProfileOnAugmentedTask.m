% plot stiffness optimizations on the augmented task

plot(pos_deg,augmented_task.motor_torque*transmission.ratio, ...
    'Color', [0 0.4 0], ...
    'DisplayName', '$\tau_a$');

lx = xlabel("$\theta [deg]$",'FontSize',12);
ly = ylabel("$[Nm]$",'FontSize',12);
set(lx,'Interpreter','Latex');
set(ly,'Interpreter','Latex');

l = legend;
set(l,'Interpreter','Latex');
l.Location = 'southeast';
l.FontSize = 14;
set(gca,'FontSize',14);
xlim_offset = (abs(max(pos_deg)) + abs(min(pos_deg)))*0.02; 
set(gca,'xlim',[min(pos_deg)-xlim_offset max(pos_deg)+xlim_offset]);